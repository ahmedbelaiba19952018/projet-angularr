import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    var firebaseConfig = {
      apiKey: "AIzaSyDiyxSzTd_WNiebZZVrgY8gPZQY76Z5uWg",
      authDomain: "http-client-demo-eb732.firebaseapp.com",
      databaseURL: "https://http-client-demo-eb732.firebaseio.com",
      projectId: "http-client-demo-eb732",
      storageBucket: "http-client-demo-eb732.appspot.com",
      messagingSenderId: "761847965842",
      appId: "1:761847965842:web:aaf8b673eb6bcbd41c9e4b",
      measurementId: "G-1617SZRXYM"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
